import React from 'react'

const Navbar = () => {
    return (
        <div className="navbar">
          <h2>My Redux Todos App</h2>  
          <ul>
              <li>Home</li>
              <li>About</li>
              <li>Total</li>              
          </ul>
        </div>
    )
}

export default Navbar
